use serde::Serialize;

#[derive(Serialize)]
pub struct MetaFile {
    pub printer_settings: PrinterSettings,
    pub total_commands: usize,
    pub toolhead_0_temperature: Option<i32>,
}

#[derive(Serialize)]
pub struct PrinterSettings {
    pub extruder: String,
    pub extruder_temperature: Option<i32>,
    pub extruder_temperatures: Vec<Option<i32>>,
}

#[derive(Serialize)]
pub struct ToolpathCommand {
    pub command: Command,
}

#[derive(Default, Serialize)]
#[serde(rename(serialize = "command"))]
pub struct Command {
    pub function: String,
    pub parameters: Param,
    pub metadata: Meta,
    pub tags: Vec<String>,
}
impl Command {
    pub fn new() -> Command {
        Default::default()
    }
}

#[derive(Serialize)]
#[serde(untagged)]
pub enum Param {
    Move {
        feedrate: f64,
        a: f64,
        x: f64,
        y: f64,
        z: f64,
    },
    ToolheadTemp {
        temperature: i32,
    },
    None {},
}
impl Default for Param {
    fn default() -> Self {
        Param::None {}
    }
}

#[derive(Serialize)]
#[serde(untagged)]
pub enum Meta {
    None {},
}
impl Default for Meta {
    fn default() -> Self {
        Meta::None {}
    }
}

#[derive(Serialize)]
pub struct MoveMetaRelative {
    pub feedrate: bool,
    pub a: bool,
    pub x: bool,
    pub y: bool,
    pub z: bool,
}
